package es.batoi.caso04_minieditor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Scanner;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author sergio
 */
public class AppController implements Initializable {

    @FXML
    private TextArea taEditor;
    @FXML
    private Label lblInfo;
    @FXML
    private Button btnAbrir;
    @FXML
    private Button btnCerrar;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnNuevo;

    private Stage escenario;
    private boolean disponible = false;
    private File f = null;
    private boolean cambiado = false;
    FileChooser fc = new FileChooser();
    Properties p = new Properties();


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fc.setInitialDirectory(new File("/home/llorenc/Escritorio"));
		taEditor.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValor, String newValor) {
                if (f != null) {
                    escenario.setTitle(f.getName() + "*");
                }
                String contenido = taEditor.getText();
                int lineas = contarLineas(contenido);
                lblInfo.setText(lineas + " líneas - " + contenido.length() + " caracteres");
                cambiado = true;
			}
		});
    }

    @FXML
    private void handleNuevo() {
        this.escenario.setTitle("Archivo sin título");
        taEditor.setText("");
    	taEditor.setDisable(false);
        System.out.println("Has pulsado en nuevo");
        disponible = true;
    }

    @FXML
    private void handleAbrir() throws IOException {

        fc.setTitle("ABRIR ARCHIVO");

        f = fc.showOpenDialog(escenario);


        if (f == null) {
            return;
        }

        p.setProperty("ruta", f.getParent());
        fc.setInitialDirectory(new File(p.getProperty("ruta")));


        FileReader fr = new FileReader(f);
        BufferedReader br = new BufferedReader(fr);
        Scanner sc = new Scanner(br);

        taEditor.setText("");
        while (sc.hasNext()) {
            taEditor.appendText(sc.nextLine() + "\n");
        }



        taEditor.setDisable(false);
        if (f != null) {
            this.escenario.setTitle(f.getName());
            System.out.println("Has abierto el archivo " + f.getAbsolutePath());
        }

        cambiado = false;


    }

    @FXML
    private void handleGuardar() throws IOException {



        if (f == null && !disponible) {
            lblInfo.setText("¿Qué intentas guardar?");
            return;
        }

        fc.setTitle("GUARDAR ARCHIVO");
        boolean nuevo = false;
        if (f == null) {
            File fileAux = fc.showSaveDialog(escenario);
            if (fileAux != null) {
                f = new File(String.valueOf(fileAux));
                p.setProperty("ruta", f.getParent());
                fc.setInitialDirectory(new File(p.getProperty("ruta")));
            }

            nuevo = true;
        }

        if (f == null) {
            return;
        }

            this.escenario.setTitle(f.getName());

            BufferedWriter bw = new BufferedWriter(new FileWriter(f));
            bw.write(taEditor.getText());
            if (nuevo) {
                lblInfo.setText("Guardado nuevo archivo en " + f.getAbsolutePath());
            } else {
                lblInfo.setText("Guardado");
            }

            this.escenario.setTitle(f.getName());
            cambiado = false;

            bw.close();




    }

    @FXML
    private void handleCerrar() throws IOException {
        BufferedWriter bw;


        System.out.println("Has pulsado en cerrar");
        if (this.escenario.getTitle() != "MINI EDITOR DE TEXTOS" ) {
            if (cambiado && f != null) {
                File fileAux = fc.showSaveDialog(escenario);

                if (fileAux != null) {
                    f = new File(String.valueOf(fileAux));
                    p.setProperty("ruta", f.getParent());
                    fc.setInitialDirectory(new File(p.getProperty("ruta")));
                    cambiado = false;
                    bw = new BufferedWriter(new FileWriter(f));
                    bw.write(taEditor.getText());
                    bw.close();
                    taEditor.setDisable(true);
                    taEditor.setText("");
                    lblInfo.setText("Cerrado y guardado");
                    this.escenario.setTitle(f.getName());
                } else {
                    return;
                }
            } else {
                taEditor.setDisable(true);
                taEditor.setText("");
                lblInfo.setText("Cerrado");
            }
        } else {
            lblInfo.setText("¿Qué intentas cerrar?");
        }

        this.escenario.setTitle("MINI EDITOR DE TEXTOS");
        f = null;
        disponible = false;
    }

    private static int contarLineas(String str){
        String[] lineas = str.split("\r\n|\r|\n");
        return  lineas.length;
    }
   

    void setEscenario(Stage escenario) {
        this.escenario = escenario;
    }


}
